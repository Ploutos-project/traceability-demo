# Complete Traceability Demo

### Scenario
Instantiate a PIE attached on the Client App.
Instantiate a PIE attached on Neuropublic WebService.
Instantiate a PIE attached on Alterra WebService.
Make a Request from the Client to get relevant info regarding a LOT Number ( This would be on a QR code upon the product )
The PIEs will communicate with each other to gather all relevant info.
Alterra WebService will receive the LOT Number and provide an identification for the Farmer that provided the product.
Neuropublic WebService will provide information regarding the Farmers Parcel / Crop / Harvets Dates / etc.
All information will be provided through the Client PIE.

### Technical Summary
A docker image will be used for the execution of the Scenario.
Composing the Image will result in the instantiation of 3 PIEs.
Upon instantiation the PIEs will be assigned a specific Port to listen for requests.
Listening Port and the PIE image will be inside the "docker-compose.yml" (Better not change them. If you do though, make sure to also change the ports in each cUrl request provided in this documentation) of our Git Clone.
The credentials for the GaiaSense API will be configured through the "env." file found inside the Git Clone.

### Required Software / Technologies
1. Docker : https://www.docker.com/
2. Git : https://git-scm.com/

### Important Notice
All cUrls in this document are formatted for Linux based machines. If you want to try them out on a Windows machine you will need to reformat them. Windows will not support new lines , single quotes , etc.

### Steps to execute the Scenario
1.	Create a folder in your system and provide a name (the name is not important)
2.	Navigate inside the newly created folder
3.	Log to the TNO servers using the following command
```bash
docker login -u "USERNAME_FOR_TNO" ci.tno.nl
```
4.	Clone the Git Repo with the following command
```bash
git clone https://ci.tno.nl/gitlab/ploutos/traceability-demo.git
```
5.	Navigate inside the cloned repo folder
6.	Rename the ".env.default" to ".env"
7.	Edit the ".env" file
8.	Update the **GAIASENSE_USERNAME** and **GAIASENSE_PASSWORD** with the correct credentials provided. Do the same for the **ALTERRA_USERNAME** and **ALTERRA_PASSWORD**.
9.	Save the ".env" file
10.	Execute the following command 
```bash
docker-compose up
```
11.	Wait till it is up and running
12.	Register a Knowledge Base in our PIE (Note: There will be no confirmation output in the console if it was successful)
```bash
curl --location --request POST 'localhost:8280/rest/sc' \
--header 'Content-Type: application/json' \
--data-raw '{
     "knowledgeBaseId": "http://www.example.org/ploutos-app",
     "knowledgeBaseName": "Ploutos Traceability App",
     "knowledgeBaseDescription": "This app offers traceability info"
}'
```
13.	Register a Graph Pattern ASK in our PIE (If Success you will receive a "**Knowledge-Interaction-Id**" which will be used in a later request. )
```bash
curl --location --request POST 'localhost:8280/rest/sc/ki' \
--header 'Knowledge-Base-Id: http://www.example.org/ploutos-app' \
--header 'Content-Type: application/json' \
--data-raw '{ "knowledgeInteractionType": "AskKnowledgeInteraction", 
  "graphPattern": "?endProduct <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <https://www.tno.nl/agrifood/ontology/ploutos/common#Product> . ?output <https://www.tno.nl/agrifood/ontology/ploutos/common#intermediateProductOf> ?endProduct . ?operation <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?operationType . ?operationType <http://www.w3.org/2000/01/rdf-schema#subClassOf> <https://www.tno.nl/agrifood/ontology/ploutos/common#ProductOperation> . ?operation <http://www.w3.org/ns/ssn/hasInput> ?input . ?operation <http://www.w3.org/ns/ssn/hasOutput> ?output . ?operation <https://www.tno.nl/agrifood/ontology/ploutos/common#responsibleAgent> ?agent . ?agent <http://www.w3.org/2004/02/skos/core#prefLabel> ?agentName . ?operation <http://semanticweb.cs.vu.nl/2009/11/sem/hasTimeStamp> ?timestamp ." 
}'
```
14.	Ask our PIE for information regarding the Specific LOT ID using the "**Knowledge-Interaction-Id**" we received from the previous request. Copy paste it in the "**Knowledge-Interaction-Id**" property of the cUrl below: 
```bash
curl --location --request POST 'localhost:8280/rest/sc/ask' \
--header 'Knowledge-Base-Id: http://www.example.org/ploutos-app' \
--header 'Knowledge-Interaction-Id: http://www.example.org/ploutos-app/interaction/c67e1a65-7cb9-45aa-87ff-8963945725cc' \
--header 'Content-Type: application/json' \
--data-raw '[{ "endProduct": "<http://sense-web.neuropublic.gr:8686/lots/130821A-L4-181011>;" }]'
```
