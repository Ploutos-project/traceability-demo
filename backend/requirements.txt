asgiref==3.5.2
certifi==2022.6.15
charset-normalizer==2.0.12
Django==3.2.9
django-cors-headers==3.10.0
django-redis==5.2.0
django-redis-cache==3.0.0
djangorestframework==3.12.4
djangorestframework-simplejwt==5.0.0
gunicorn==20.1.0
idna==3.3
knowledge-mapper==0.0.19
mysql-connector-python==8.0.30
protobuf==3.20.1
psycopg2-binary==2.9.2
PyJWT==2.4.0
pytz==2022.2.1
redis==3.5.3
requests==2.26.0
six==1.16.0
sqlparse==0.4.2
urllib3==1.26.12
whitenoise==5.3.0
