from core.services.pie import PIEService
from rest_framework import status, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

pie_service = None

def check_pie_service():
    global pie_service
    if pie_service is None:
        pie_service = PIEService()

class PIEVIewSet(viewsets.ViewSet):
    http_method_names = ["get", "post", "head"]
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    @action(detail=True, methods=["get"])
    def get_lot(self, request, pk=None):
        check_pie_service()
        user_type = request.user.groups.first().name
        return Response(pie_service.ask_for_lot(pk, user_type))

    @action(detail=False, methods=["post"])
    def get_parcel(self, request):
        check_pie_service()
        user_type = request.user.groups.first().name
        return Response(pie_service.ask_for_soil_operations(user_type, request.data[0]["parcel"]))
