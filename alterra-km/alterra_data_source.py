import requests
from requests.auth import HTTPBasicAuth
import os
import re
from uuid import uuid4
import logging as log
from urllib.parse import urlparse
import pandas as pd
import numpy as np
from datetime import datetime
import urllib.parse
from knowledge_mapper.data_source import DataSource
from knowledge_mapper.knowledge_interaction import AskKnowledgeInteraction

ALTERRA = "https://example.org/alterra"
PACKAGING = "https://www.tno.nl/agrifood/ontology/ploutos/common#PackagingOperation"
class AlterraDataSource(DataSource):
    def __init__(self, url):
        self.url = urlparse(url)

        # We use the dataframes in this dict to quickly return results so that
        # we don't have to fetch them from the gaiasense API, and to make it
        # easier to have a semantically 'pure' implementation of the knowledge
        # interaction handlers.
        self.caches = dict()

        # Read the username and password from environment variables. You should
        # set these in `.env` in the docker-compose project. (Or pass them in
        # another way in the live version.)
        if 'ALTERRA_USERNAME' in os.environ and 'ALTERRA_PASSWORD' in os.environ:
            self.username = os.environ['ALTERRA_USERNAME']
            self.password = os.environ['ALTERRA_PASSWORD']

        # In this dict, we will collect all packaging operation  URIs that we
        # know of. For these, we can also provide phase two data.
        self.packaging_operations = dict()

        # Will be set lazily when handling a KI that needs it.
        self.ggn = None


    def test(self):
        response = requests.get(
            f'{self.url.geturl()}',
            auth=HTTPBasicAuth(self.username, self.password)
        )
        if not response.ok:
            raise Exception(f'Unexpected status while testing {self.url.geturl()}: {response.status_code}')
        else:
            log.info(f'Successfully tested {self.url.geturl()}.')
        
        response = requests.get(
            f'{self.url.geturl()}/profile',
            headers={
                'accept': 'application/json'
            },
            auth=HTTPBasicAuth(self.username, self.password)
        )

        if not response.ok:
            raise Exception(f'Unexpected status while getting profile: {response.status_code}: {response.text}')
        else:
            self.profile = response.json()


    def handle(self, ki, binding_set, requesting_kb):
        """Handle the `binding_set` with incoming bindings, for the knowledge interaction `ki`."""
        log.info(f'incoming: {binding_set}')
        if ki['pattern'] not in self.caches:
            # Make a data frame for this KI's cache.
            self.caches[ki['pattern']] = pd.DataFrame(columns=ki['vars'])
        cache = self.caches[ki['pattern']]

        if ki['name'] == 'chain':
            result = self.handle_chain(ki, binding_set, cache)
        elif ki['name'] == 'packaging-operation':
            result = self.handle_packaging_operation(ki, binding_set)
        elif ki['name'] == 'global-gap-number':
            result = self.handle_global_gap_number(ki, binding_set)
            

        log.info(f'outgoing: {result}')

        return result

    def get_lot_id(self, iri):
        """Returns the parcel ID from the given IRI. E.g.,
        "<http://sense-web.neuropublic.gr:8686/lots/hjk27ha2>" returns
        "hjk27ha2"."""
        matches = re.findall(f"<{re.escape(f'{self.url.scheme}://{self.url.netloc}/lots/')}([^>]+)>", iri)
        if matches:
            return matches[0]
        else:
            return None


    def make_lot_iri(self, lot_id):
        """Inverse of `get_lot_id`."""
        return f"<{self.url.scheme}://{self.url.netloc}/lots/{lot_id}>"


    def make_bin_iri(self, bin_id, parcel_id, import_date):
        return f"<http://sense-web.neuropublic.gr:8585/bins/{bin_id}/{parcel_id}/{import_date.strftime('%Y-%m-%d')}>"


    def get_ggn(self):
        if self.ggn is None:
            response = requests.get(
                f'{self.url.geturl()}/profile',
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )
            self.ggn = response.json()['cert_authority_identifier']
        return self.ggn


    def handle_chain(self, ki, binding_set, cache):
        print(cache)
        result_bindings = []
        new_from_api = []
        if not binding_set:
            binding_set.append(dict())
        for incoming_binding in binding_set:
            cached_fttc = get_matching_cached_bindings(cache, incoming_binding)
            if cached_fttc is not None and len(cached_fttc) > 0:
                log.info('using cached bindings')
                result_bindings += cached_fttc
                continue

            if 'endProduct' in incoming_binding:
                lot_id = self.get_lot_id(incoming_binding['endProduct'])
                if lot_id is None:
                    # Since this KB is not able to retrieve the endProduct, it
                    # must be a product that we're unaware of, so we can safely
                    # skip it.
                    continue
            else:
                log.warning('This knowledge mapper needs a binding for "endProduct"! Skipping it now...')
                continue

            unsupported_bindings = set(incoming_binding.keys()) - {'endProduct'}
            if unsupported_bindings:
                log.warning(f'There are incoming bindings that are not supported and were ignored: {unsupported_bindings}.')

            response = requests.get(
                f'{self.url.geturl()}/lots/{lot_id}',
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )

            # Parse the response from the API, and translate it into appropriate
            # bindings.
            # ["endProduct", "input", "output", "operation", "operationType", "agent", "agentName", "timestamp"]
            for bin in response.json():
                bin_id = bin['bin_id']
                producer = bin["producer"]
                timestamp = bin["import_date"]
                outgoing_binding = {
                    'endProduct': self.make_lot_iri(lot_id),
                    'input': self.make_bin_iri(int(bin_id), int(bin["parcelid"]), datetime.fromisoformat(bin["import_date"])),
                    'output': self.make_lot_iri(lot_id),
                    'operation': f'<{self.blank_node_iri()}>',
                    'operationType': f'<{PACKAGING}>',
                    'agent': f'<{ALTERRA}>',
                    'agentName': f'"{self.profile["name"]}"',
                    'timestamp': f'"{timestamp}"^^<http://www.w3.org/2001/XMLSchema#dateTime>'
                }
                add_to_cache(cache, outgoing_binding)

                # Save additional data for this generated IRI, so that we can
                # answer phase-two data later.
                country, country_label = self.map_country(bin['country'])
                self.packaging_operations[outgoing_binding['operation']] = {
                    'country': country,
                    'country_label': country_label,
                }

                result_bindings.append(outgoing_binding)
                new_from_api.append(outgoing_binding)

        if self.kb is not None:
            chain_ask: AskKnowledgeInteraction = self.kb.get_ki(name='chain-ask')
            bindings_for_further_down_the_chain = []

            for new_binding_from_api in new_from_api:
                original_end_product = new_binding_from_api['endProduct']
                binding_ffttc = {
                    'endProduct': new_binding_from_api['input']
                }

                cached_ffttc = get_matching_cached_bindings(cache, binding_ffttc)
                if cached_ffttc is not None and len(cached_ffttc) > 0:
                    log.info('using cached bindings (ffttc)')
                    result_bindings += cached_ffttc
                else:
                    bindings_for_further_down_the_chain.append(binding_ffttc)

            if bindings_for_further_down_the_chain:
                log.info(f'for further down chain: {bindings_for_further_down_the_chain}')
                new_bindings_ffttc = chain_ask.ask(bindings_for_further_down_the_chain)['bindingSet']
                log.info(f'from further down chain: {new_bindings_ffttc}')

                for binding_from_down_the_chain in new_bindings_ffttc:
                    binding_from_down_the_chain['endProduct'] = original_end_product
                    add_to_cache(cache, binding_from_down_the_chain)

                result_bindings += new_bindings_ffttc

        return result_bindings


    # See https://www.w3.org/TR/rdf11-concepts/#section-skolemization
    def blank_node_iri(self):
        return f'{self.url.scheme}://{self.url.netloc}/.well-known/genid/{uuid4()}'


    def map_country(self, country):
        if country == 'GR':
            iri, label = ('http://publications.europa.eu/resource/authority/country/GRC', 'Greece')
        else:
            iri, label = (self.hash_iri(country), country)
            log.warn(f"Unknown country '{country}'. Generated placeholder IRI {iri} with label '{country}'.")
        return iri, label


    def handle_packaging_operation(self, ki, binding_set):
        result_bindings = []
        for incoming_binding in binding_set:

            if 'operation' in incoming_binding:
                if incoming_binding['operation'] not in self.packaging_operations:
                    # This is not an operation that we're aware of, so we don't
                    # produce a result binding.
                    continue;
            else:
                log.warning('This knowledge mapper needs a binding for "operation"! Skipping it now...')
                continue

            unsupported_bindings = set(incoming_binding.keys()) - {'operation'}
            if unsupported_bindings:
                log.warning(f'There are incoming bindings that are not supported and were ignored: {unsupported_bindings}.')

            latitude = f"{self.profile['latitude']}"
            longitude = f"{self.profile['longitude']}"
            location = f'<{self.url.scheme}://{self.url.netloc}/locations/{urllib.parse.quote_plus(latitude)}/{urllib.parse.quote_plus(longitude)}>'

            new_result_binding = {
                'operation': incoming_binding['operation'],
                'agent': f'<{ALTERRA}>',
                'location': location,
                'latitude': f'{latitude}',
                'longitude': f'{longitude}',
                'country': f'<{self.packaging_operations[incoming_binding["operation"]]["country"]}>',
                'countryLabel': f'"{self.packaging_operations[incoming_binding["operation"]]["country_label"]}"',
            }
            result_bindings.append(new_result_binding)
        return result_bindings

    def handle_global_gap_number(self, ki, binding_set):
        result_bindings = []
        for incoming_binding in binding_set:
            log.info(f'incoming binding {incoming_binding}.')
            # check whether the binding contains the ID of the organisation to which this KM belongs
            if 'organisation' in incoming_binding:
                if incoming_binding['organisation'] != f'<{ALTERRA}>':
                    # The  is not an the correct organisation, so we don't produce a result binding.
                    log.info(f'organisation is not Alterra.')
                    continue;
            else:
                log.warning('This knowledge mapper needs a binding for "organisation"! Skipping it now...')
                continue
            # at this point the organisation is Alterra, so we need to return the GGN of Alterra
            new_result_binding = {
                'organisation': f'<{ALTERRA}>',
                'ggn': f'"{self.get_ggn()}"',
            }
            result_bindings.append(new_result_binding)
        return result_bindings

def add_to_cache(cache: pd.DataFrame, binding):
    binding_as_row = cache.columns.map(lambda variable: binding[variable])
    cache.loc[-1] = binding_as_row
    cache.index += 1
    cache.sort_index(inplace=True)


def get_matching_cached_bindings(cache, incoming_binding):
    requirements = pd.Series(np.ones(shape=len(cache), dtype=bool))
    for variable_name, variable_value in incoming_binding.items():
        requirements = requirements & (cache[variable_name] == variable_value)
    
    matches = cache[requirements]
    return matches.to_dict(orient='records')
