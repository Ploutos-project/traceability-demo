import requests
from requests.auth import HTTPBasicAuth
import os
import re
from uuid import uuid4
import logging as log
from datetime import datetime
from urllib.parse import urlparse, quote_plus
import pandas as pd
import numpy as np
import hashlib
from knowledge_mapper.data_source import DataSource
import shapely.wkt
from pyproj import Geod

class GaiaSenseDataSource(DataSource):
    def __init__(self, url):
        self.url = urlparse(url)

        # We use the dataframes in this dict to quickly return results so that
        # we don't have to fetch them from the gaiasense API, and to make it
        # easier to have a semantically 'pure' implementation of the knowledge
        # interaction handlers.
        self.caches = dict()

        # Read the username and password from environment variables. You should
        # set these in `.env` in the docker-compose project. (Or pass them in
        # another way in the live version.)
        if 'GAIASENSE_USERNAME' in os.environ and 'GAIASENSE_PASSWORD' in os.environ:
            self.username = os.environ['GAIASENSE_USERNAME']
            self.password = os.environ['GAIASENSE_PASSWORD']
        
        # In this dict, we will store the IRIs of the harvesting operations that
        # we generate. Each of those will map to some data that we can use to
        # fetch more detailed data (phase 2) later.
        self.harvesting_operations = dict()


    def test(self):
        response = requests.get(
            f'{self.url.geturl()}',
            auth=HTTPBasicAuth(self.username, self.password)
        )
        if not response.ok:
            raise Exception(f'Unexpected status while testing {self.url.geturl()}: {response.status_code}')
        else:
            log.info(f'Successfully tested {self.url.geturl()}.')


    def handle(self, ki, binding_set, requesting_kb):
        """Handle the `binding_set` with incoming bindings, for the knowledge
        interaction `ki`."""
        log.info(f'incoming: {binding_set}')
        log.info(f'triggering ki: {ki["name"]}')

        if ki['pattern'] not in self.caches:
            # Make a data frame for this KI's cache.
            self.caches[ki['pattern']] = pd.DataFrame(columns=ki['vars'])
        cache = self.caches[ki['pattern']]

        # Based on the name (given in `config.json` we distinguish which
        # knowledge interaction we're dealing with, and call the correct
        # method.)
        if ki['name'] == 'chain':
            result_bindings =  self.handle_chain(ki, binding_set, cache)
        elif ki['name'] == 'harvesting-operation':
            result_bindings = self.handle_harvesting_operation(ki, binding_set, cache)
        elif ki['name'] == 'soil-mgmt-operations':
            result_bindings = self.handle_soil_mgmt_operations(ki, binding_set, cache)

        log.info(f'outgoing: {result_bindings}')

        return result_bindings


    def parse_parcel_iri(self, iri):
        """Returns the parcel ID from the given IRI. E.g.,
        "http://sense-web.neuropublic.gr/parcels/188451" returns "188451"."""
        matches = re.findall(f"<{re.escape(f'{self.url.scheme}://{self.url.netloc}/parcels/')}(\\d+)>", iri)
        if matches:
            return matches[0]
        else:
            return None


    def make_parcel_iri(self, parcel_id):
        """Inverse of `parse_parcel_iri`."""
        return f"{self.url.scheme}://{self.url.netloc}/parcels/{parcel_id}"


    def parse_farm_iri(self, iri):
        """Returns the parcel ID from the given IRI. E.g.,
        "http://sense-web.neuropublic.gr/farms/434" returns "434"."""
        matches = re.findall(f"{re.escape(f'{self.url.scheme}://{self.url.netloc}/farms/')}(\\d+)", iri)
        if matches:
            return matches[0]
        else:
            return None


    def make_farm_iri(self, locationid):
        """Inverse of `parse_location_iri`."""
        return f"{self.url.scheme}://{self.url.netloc}/farms/{locationid}"


    def make_agent_iri(self, name):
        return f'{self.url.scheme}://{self.url.netloc}/agents/{hashlib.md5(name.encode("utf-8")).hexdigest()}'


    def parse_bin_iri(self, iri):
        """Returns the bin_id, parcel_id, and import_date from the given IRI. E.g.,
        "<http://sense-web.neuropublic.gr:8585/bins/1234/5678/2020-01-02>" returns
        (1234, 5678, datetime.fromisoformat("2020-01-02"))."""
        matches = re.findall(f"<{re.escape(f'{self.url.scheme}://{self.url.netloc}')}/bins/([^>]+)/([^>]+)/([^>]+)>", iri)
        if matches:
            match = matches[0]
            return (int(match[0]), int(match[1]), datetime.fromisoformat(match[2]))
        else:
            return None

    def make_bin_iri(self, bin_id, parcel_id, import_date):
        return f"<http://sense-web.neuropublic.gr:8585/bins/{bin_id}/{parcel_id}/{import_date.strftime('%Y-%m-%d')}>"

    def handle_chain(self, ki, binding_set, cache):
        # - Split the binding_set into three lists
        #   1. The bindings that we have a cached response to
        #   2. The bindings that we can get a new response to (with valid bin ID in `endProduct`, and no other bindings (?))
        #   3. The bindings that we cannot answer to (we ignore these)
        # - From set (1) we can include the cached bindings in the `result_bindings`
        # - From set (2) we prepare a single request where we include all bin IDs
        #   - For this, we also need the `parcelid` and `import_date`, I can parse these from the IRI...
        #   - The result of this request must be processed into valid bindings, and included in `result_bindings`

        cached_results, not_cached = [], []
        for b in binding_set:
            cached_result = get_matching_cached_bindings(cache, b)
            if len(cached_result) == 0:
                not_cached.append(b)
            else:
                if 'endProduct' in b:
                    cached_results += cached_result
                    if len(b) > 1:
                        log.warning(f'Ignored variables in binding (only endProduct considered): {b}')
                else:
                    log.warning(f'Binding must include endProduct: {b}');

        post_data = []
        original_bin_iris = []
        new_result_bindings = []
        bin_to_iri = dict()
        for b in not_cached:
            try:
                bin_id, parcel_id, import_date = self.parse_bin_iri(b['endProduct'])
                post_data.append({
                    'bin_id': bin_id,
                    'parcelid': parcel_id,
                    'import_date': import_date.strftime('%Y-%m-%d'),
                })
                bin_to_iri[bin_id] = b['endProduct']
                original_bin_iris.append(b['endProduct'])
            except (TypeError, ValueError):
                log.warning(f'Ignoring binding with endProduct IRI that is unknown to us: {b}')

        if post_data:
            log.info(f'sending to GaiaSense web service: {post_data}')
            response = requests.post(
                f'{self.url.geturl()}/bins',
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password),
                json=post_data,
            )

            if not response.ok:
                log.warning(f'request failed with status {response.status_code} and body "{response.text}"')
            else:
                log.info(f'received from GaiaSense web service: {response.json()}')
                for bin in response.json():
                    # TODO: Because we cannot match the input and output of the
                    # request, we no rely on that there's hopefully no duplicate
                    # bin id's in a single request..
                    bin_iri = bin_to_iri[int(bin['bucketCode'])]
                    new_result_binding = {
                        'endProduct': bin_iri,
                        'input': f'<{self.blank_node_iri()}>',
                        'output': bin_iri,
                        'operation': f'<{self.blank_node_iri()}>',
                        'operationType': '<https://www.tno.nl/agrifood/ontology/ploutos/common#HarvestingOperation>',
                        'agent': f'<{self.make_agent_iri(bin["adminSubscriber"])}>',
                        'agentName': f'"{bin["adminSubscriber"]} (GR)"',
                        'timestamp': f'"{bin["harvestDate"]}"^^<http://www.w3.org/2001/XMLSchema#dateTime>'
                    }
                    new_result_bindings.append(new_result_binding)
                    # Save some data that we have to use when more info about
                    # this operation is asked later.
                    self.harvesting_operations[new_result_binding['operation']] = {
                        'parcel_id': bin['parcelId'],
                        'location_id': bin['locationId'],
                        'output': bin_iri,
                    }
                    add_to_cache(cache, new_result_binding)

        return cached_results + new_result_bindings


    def handle_harvesting_operation(self, ki, binding_set, cache):
        result_bindings = []
        for binding in binding_set:
            cached_bindings = get_matching_cached_bindings(cache, binding)
            if cached_bindings is not None and len(cached_bindings) > 0:
                result_bindings += cached_bindings
                continue

            if 'operation' not in binding:
                continue

            operation = binding['operation']

            if operation not in self.harvesting_operations:
                continue

            parcel_id = self.harvesting_operations[operation]['parcel_id']
            location_id = self.harvesting_operations[operation]['location_id']
            output = self.harvesting_operations[operation]['output']

            response = requests.get(
                f'{self.url.geturl()}/locations/agronomy',
                params={
                    'parcelid': parcel_id,
                    'lang': 'en'
                },
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )

            # TODO: Validate response for errors/expectations

            agro = response.json()[0]

            # Parse polygon and compute its area.
            polygon = shapely.wkt.loads(agro['polygon'])
            geod = Geod(ellps='WGS84')
            area_and_perimiter = geod.polygon_area_perimeter(
                [c[0] for c in polygon.exterior.coords], # lats
                [c[1] for c in polygon.exterior.coords]  # longs
            )

            parcel = f'<{self.make_parcel_iri(parcel_id)}>'

            crop_type, crop_type_label = self.map_crop(agro['rocrDescription'])
            crop = f'<{self.blank_node_iri()}>'

            latitude = f"{agro['latitude']}"
            longitude = f"{agro['longitude']}"
            location = f'<{self.url.scheme}://{self.url.netloc}/locations/{quote_plus(latitude)}/{quote_plus(longitude)}>'

            farm_name = f'"{agro["producerName"]}"'
            farm = f'<{self.make_farm_iri(location_id)}>'
            farmer_name = agro['producerName']
            farmer = f'<{self.url.scheme}://{self.url.netloc}/farmers/{hashlib.md5(farmer_name.encode("utf-8")).hexdigest()}>'
            association_name = f"\"{agro['adminSubscriber']}\""
            association = f'<{self.url.scheme}://{self.url.netloc}/associations/{hashlib.md5(association_name.encode("utf-8")).hexdigest()}>'

            purpose, purpose_label = self.map_purpose(agro['productDirection'])

            country, country_label = self.map_country(agro['country'])

            new_result_binding = {
                'operation': operation,
                'output': output,
                'parcel': parcel,
                'crop': crop,
                'cropType': f'<{crop_type}>',
                'cropName': f'"{crop_type_label}"',
                'area': area_and_perimiter[0],
                'location': location,
                'latitude': latitude,
                'longitude': longitude,
                'farmName': farm_name,
                'farm': farm,
                'farmer': farmer,
                'association': association,
                'associationName': association_name,
                'parcelAdminName': f'"{" ".join([agro["adminFirstname"], agro["adminLastname"]])}"',
                'parcelToponym': f'"{agro["parcelToponym"]}"',
                'parcelRegion': f'"{agro["parcelPrefecture"]}"',
                'purpose': f'"{purpose_label}"',
                'cultivatorName': f'"{agro["producerName"]}"',
                'country': f'<{country}>',
                'countryLabel': f'"{country_label}"',
            }
            result_bindings.append(new_result_binding)
            add_to_cache(cache, new_result_binding)
        return result_bindings


    def handle_soil_mgmt_operations(self, ki, binding_set, cache):
        result_bindings = []
        if not binding_set:
            # We add the empty binding to the set. This way, we will still
            # retrieve things from the cache.
            binding_set.append(dict())

        for binding in binding_set:
            cached_bindings = get_matching_cached_bindings(cache, binding)
            if cached_bindings is not None and len(cached_bindings) > 0:
                log.info('using cached bindings')
                result_bindings += cached_bindings
                continue

            params = {
                # We set this date to make sure we fetch all sprays if no
                # incoming date is received.
                'fromdate': '1900-01-01'
            }
            if 'parcel' in binding:
                parcel_id = self.parse_parcel_iri(binding['parcel'])
                if parcel_id is not None:
                    params['parcelid'] = parcel_id
                else:
                    # Since this KB is not able to retrieve the parcelid, it
                    # must be a parcel that we're unaware of, so we can safely
                    # skip it.
                    continue
            else:
                # If there is no bindings given, this KB currently won't fetch
                # ALL parcels from /locations and then make a request for each
                # of them to /locations/events/{sprays,...}. That will probably
                # be too resource-intensive.
                continue

            unsupported_bindings = set(binding.keys()) - {'parcel'}
            if unsupported_bindings:
                log.warning(f'There are incoming bindings that are not supported and were ignored: {unsupported_bindings}.')

            # First, get some agronomy info about the parcel
            response = requests.get(
                f'{self.url.geturl()}/locations/agronomy',
                params={
                    'parcelid': parcel_id,
                    'lang': 'en'
                },
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )

            # TODO: Validate response for errors/expectations

            agro = response.json()[0]

            polygon = shapely.wkt.loads(agro['polygon'])
            geod = Geod(ellps='WGS84')
            area_and_perimiter = geod.polygon_area_perimeter(
                [c[0] for c in polygon.exterior.coords], # lats
                [c[1] for c in polygon.exterior.coords]  # longs
            )
            area = area_and_perimiter[0]

            log.info(f'doing response to {self.url.geturl()}/locations/events/sprays with params: {params}')
            response = requests.get(
                f'{self.url.geturl()}/locations/events/sprays',
                params=params,
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )
            for spray in response.json():
                material, material_label = self.map_pesticide(spray['drugName'], spray['activeSubstance'])

                outgoing_binding = {
                    # We just copy the incoming parcel binding.. This may change
                    # in the future since we may be mixing things up with
                    # locations/parcels
                    'operation': f'<{self.blank_node_iri()}>',
                    'soilManagementOperationType': '<https://www.tno.nl/agrifood/ontology/ploutos/common#CropProtectionOperation>',
                    'parcel': binding['parcel'],
                    'applied': f'<{self.blank_node_iri()}>',
                    'appliedUnit': f'<{self.map_unit(spray["unit"])}>',
                    'appliedValue': spray['dose'],
                    'area': f'<{self.blank_node_iri()}>',
                    'areaUnit': '<http://www.ontology-of-units-of-measure.org/resource/om-2/squareMetre>',
                    'areaValue': f'{area}',
                    # Since the API only returns a single date, we can assume
                    # that it starts and ends on that date. (No time included.)
                    'start': f'"{spray["date"]}"^^<http://www.w3.org/2001/XMLSchema#date>',
                    'end': f'"{spray["date"]}"^^<http://www.w3.org/2001/XMLSchema#date>',
                    'agent': f'<{self.make_agent_iri(agro["adminSubscriber"])}>',
                    'material': f'<{material}>',
                    'activeSubstance': f'"{material_label}"',
                }

                result_bindings.append(outgoing_binding)
                add_to_cache(cache, outgoing_binding)
            
            log.info(f'doing response to {self.url.geturl()}/locations/events/irrigations with params: {params}')
            response = requests.get(
                f'{self.url.geturl()}/locations/events/irrigations',
                params=params,
                headers={
                    'accept': 'application/json'
                },
                auth=HTTPBasicAuth(self.username, self.password)
            )
            for irri in response.json():
                dose_raw = irri['waterQuantity']
                unit_raw = irri['unit']

                if unit_raw.strip() == 'm3/Στρέμμα':
                    # dose_raw is the dose PER 1000 m2. We need to translate
                    # that to the dose for the parcel area.
                    dose = float(dose_raw) * (area / 1000)
                    unit = self.map_unit('m3')
                else:
                    log.warning(f'unknown unit for irrigation: {unit_raw}')
                    continue
                
                start_dt = datetime.strptime(irri['startDateTime'], "%Y-%m-%d %H:%M:%S")
                end_dt = datetime.strptime(irri['endDateTime'], "%Y-%m-%d %H:%M:%S")

                material_label = 'Water'
                material = 'https://www.tno.nl/agrifood/ontology/ploutos/common#water'

                outgoing_binding = {
                    # We just copy the incoming parcel binding.. This may change
                    # in the future since we may be mixing things up with
                    # locations/parcels
                    'operation': f'<{self.blank_node_iri()}>',
                    'soilManagementOperationType': '<https://www.tno.nl/agrifood/ontology/ploutos/common#IrrigationOperation>',
                    'parcel': binding['parcel'],
                    'applied': f'<{self.blank_node_iri()}>',
                    'appliedUnit': f'<{unit}>',
                    'appliedValue': f'{dose}',
                    'area': f'<{self.blank_node_iri()}>',
                    'areaUnit': '<http://www.ontology-of-units-of-measure.org/resource/om-2/squareMetre>',
                    'areaValue': f'{area}',
                    # Since the API only returns a single date, we can assume
                    # that it starts and ends on that date. (No time included.)
                    'start': f'"{start_dt.isoformat()}"^^<http://www.w3.org/2001/XMLSchema#dateTime>',
                    'end': f'"{end_dt.isoformat()}"^^<http://www.w3.org/2001/XMLSchema#dateTime>',
                    'agent': f'<{self.make_agent_iri(agro["adminSubscriber"])}>',
                    'material': f'<{material}>',
                    'activeSubstance': f'"{material_label}"',
                }

                result_bindings.append(outgoing_binding)
                add_to_cache(cache, outgoing_binding)

        return result_bindings


    # See https://www.w3.org/TR/rdf11-concepts/#section-skolemization
    def blank_node_iri(self):
        return f'{self.url.scheme}://{self.url.netloc}/.well-known/genid/{uuid4()}'

    def hash_iri(self, source):
        return f'{self.url.scheme}://{self.url.netloc}/.well-known/genid/{hashlib.md5(source.encode("utf-8")).hexdigest()}'


    def map_crop(self, rocr_description):
        """Returns a tuple with the IRI of the concept and a label. (Ideally
        this should be read from a config file, or even queried to the PCSM or
        some other vocabulary."""
        if rocr_description == 'rodakinia':
            return ('http://purl.obolibrary.org/obo/NCBITaxon_3760', 'Prunus persica')
        else:
            log.warning(f'Unknown rocr_description: "{rocr_description}" (add to ontology/mapper?)')
            return ('https://www.tno.nl/agrifood/ontology/ploutos/common#Crop', rocr_description)


    def map_pesticide(self, drug_name, active_substance):
        if active_substance is None:
            active_substance = ''
        # TODO: Use concepts from the PCSM...
        return (self.hash_iri(drug_name), active_substance)

    def map_purpose(self, direction):
        if direction == 'industrial use':
            return ('https://www.tno.nl/agrifood/ontology/ploutos/common#industrialUse', 'Industrial use') # TODO: Validate with PCSM


    def map_unit(self, unit):
        if unit == 'gr':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/gram'
        elif unit == 'kg':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/kilogram'
        elif unit == 'cm3':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/cubicCentimetre'
        elif unit == 'ml':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/millilitre'
        elif unit == 'lt':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/litre'
        elif unit == 'm3':
            return 'http://www.ontology-of-units-of-measure.org/resource/om-2/cubicMetre'
        else:
            log.warning(f'Unknown unit: "{unit}" (add to mapping?)')
    
    def map_country(self, country):
        if country == 'GR':
            iri, label = ('http://publications.europa.eu/resource/authority/country/GRC', 'Greece')
        else:
            iri, label = (self.hash_iri(country), country)
            log.warn(f"Unknown country '{country}'. Generated placeholder IRI {iri} with label '{country}'.")
        return iri, label


def add_to_cache(cache: pd.DataFrame, binding):
    binding_as_row = cache.columns.map(lambda variable: binding[variable])
    cache.loc[-1] = binding_as_row
    cache.index += 1
    cache.sort_index(inplace=True)


def get_matching_cached_bindings(cache, incoming_binding):
    requirements = pd.Series(np.ones(shape=len(cache), dtype=bool))
    log.info(cache)
    log.info(incoming_binding)
    for variable_name, variable_value in incoming_binding.items():
        requirements = requirements & (cache[variable_name] == variable_value)
    
    matches = cache[requirements]
    return matches.to_dict(orient='records')
