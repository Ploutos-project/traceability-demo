docker-compose up -d prdd
echo "waiting while prdd is starting..."
sleep 15

docker-compose up -d app-pie gaiasense-pie alterra-pie certificate-pie

docker-compose up -d gaiasense-km
echo "waiting while gaiasense-km is starting..."
sleep 15

docker-compose up -d alterra-km
echo "waiting while alterra-km is starting..."
sleep 15

docker-compose up -d certificate-km
echo "waiting while certificate-km is starting..."
sleep 15

docker-compose up -d app-km app-km-processor
echo "waiting while app-km and app-km-processor are starting..."
sleep 15

docker-compose up -d db
docker-compose up -d app