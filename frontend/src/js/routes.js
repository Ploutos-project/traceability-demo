
import HomePage from '../pages/home.vue';
import LotPage from '../pages/lot.vue';
import DetailPage from '../pages/detail.vue';
import CertificatesPage from '../pages/certificates.vue';
import QRScanPage from '../pages/qrscan.vue';
import LoginPage from '../pages/login.vue';

var routes = [
  {
    path: '/',
    component: LoginPage,
  },
  {
    path: '/home/',
    component: HomePage,
  },
  {
    path: '/lot/',
    component: LotPage,
  },
  {
    path: '/detail/',
    component: DetailPage,
  },
  {
    path: '/certificates/',
    component: CertificatesPage,
  },
  {
    path: '/qrscan/',
    component: QRScanPage,
  },
  {
    path: '(.*)',
    component: LoginPage,
  },
];

export default routes;
