import { f7 } from "framework7-vue";
import store from "../js/store";

// configurable during build by setting env variable
const backend_endpoint = import.meta.env.VITE_PLOUTOS_BACKEND ?? "https://back.ploutos-docker.hispatecanalytics.com"

export default function getParcel(parcel) {
    //function to just get the outputs of the desired lot
    fetch(`${backend_endpoint}/api/v1/pie/get_parcel/`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8',
            'Authorization': `Token ${store.state.token}`
        },
        body: JSON.stringify([{'parcel': parcel}])
    }).then(response => {
        return response.json();
      }).then(data => {
        if (data.length > 0) {
            store.state.soil_operations = data[0];
            f7.preloader.hide();
            f7.views.main.router.navigate('/detail/', { reloadCurrent: true });
        } else {
          f7.views.main.router.navigate('/lot/', { reloadCurrent: true });
          f7.preloader.hide();
          f7.dialog.alert("Wrong lot id");
        }
      }).catch(err => {
        console.log(err)
      });
}
