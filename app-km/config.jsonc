{
  // The endpoint where a knowledge engine is available.
  // "knowledge_engine_endpoint": "http://app-pie:8280/rest", // This one is configured in the environment variable KE_ENDPOINT
  "knowledge_base": {
    // An URL representing the identity of this knowledge base
    "id": "http://www.example.org/ploutos-app",
    // A name for this knowledge base
    "name": "Ploutos Traceability App",
    // A description for this knowledge base
    "description": "This app offers traceability info"
  },
  // This property tells the Knowledge Mapper that we want to use a custom
  // Python class as the data source.
  "plugin": {
    "class": "ploutos_app_knowledge_mapper.PloutosAppDataSource"
  },
  // Several knowledge interaction definitions can be placed here.
  "knowledge_interactions": [
    {
      "name": "ask-chain",
      "type": "ask",
      "prefixes": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "ssn": "http://www.w3.org/ns/ssn/",
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
        "s4agri": "https://saref.etsi.org/saref4agri/"
      },
      "vars": [
        "endProduct",
        "input",
        "output",
        "operation",
        "operationType",
        "agent",
        "agentName",
        "timestamp"
      ],
      "pattern": "?endProduct rdf:type ploutos:Product . ?output ploutos:intermediateProductOf ?endProduct . ?operation rdf:type ?operationType . ?operationType rdfs:subClassOf ploutos:ProductOperation . ?operation ploutos:hasResponsibleAgent ?agent . ?agent s4agri:hasName ?agentName . ?operation ssn:hasInput ?input . ?operation ssn:hasOutput ?output . ?operation ploutos:hasEndDatetime ?timestamp ."
    },
    {
      "name": "ask-packaging-operations",
      "type": "ask",
      "prefixes": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
        "wgs84": "http://www.w3.org/2003/01/geo/wgs84_pos#",
        "foaf": "http://xmlns.com/foaf/0.1/"
      },
      "vars": ["operation", "agent", "location", "latitude", "longitude", "country", "countryLabel"],
      "pattern": "?operation rdf:type ploutos:PackagingOperation . ?operation ploutos:hasResponsibleAgent ?agent . ?agent wgs84:location ?location . ?location wgs84:lat ?latitude . ?location wgs84:long ?longitude . ?agent ploutos:hasCountry ?country . ?country foaf:name ?countryLabel ."
    },
    {
      "name": "ask-harvesting-operations",
      "type": "ask",
      "prefixes": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "ssn": "http://www.w3.org/ns/ssn/",
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
        "s4agri": "https://saref.etsi.org/saref4agri/",
        "wgs84": "http://www.w3.org/2003/01/geo/wgs84_pos#",
        "foaf": "http://xmlns.com/foaf/0.1/"
      },
      "vars": ["operation", "output", "parcel", "crop", "cropType", "cropName", "area", "location", "latitude", "longitude", "farmName", "farm", "farmer", "association", "associationName", "parcelAdminName", "parcelToponym", "parcelRegion", "purpose", "cultivatorName", "country", "countryLabel"],
      "pattern": "?operation rdf:type ploutos:HarvestingOperation . ?operation ssn:hasOutput ?output . ?operation ploutos:isOperatedOn ?parcel . ?parcel s4agri:contains ?crop . ?crop rdf:type ?cropType . ?cropType rdfs:label ?cropName . ?parcel ploutos:hasArea ?area . ?farm rdf:type ploutos:Farm . ?farm wgs84:location ?location . ?location wgs84:lat ?latitude . ?location wgs84:long ?longitude . ?farm s4agri:hasName ?farmName . ?farm s4agri:contains ?parcel . ?farmer s4agri:managesFarm ?farm . ?association rdf:type ploutos:FarmAssociation . ?farmer s4agri:isMemberOf ?association . ?association s4agri:hasName ?associationName . ?parcel ploutos:hasAdministrator ?parcelAdminName . ?parcel ploutos:hasToponym ?parcelToponym . ?parcel ploutos:inRegion ?parcelRegion . ?output ploutos:isMeantFor ?purpose . ?parcel ploutos:hasCultivator ?cultivatorName . ?farm ploutos:hasCountry ?country . ?country foaf:name ?countryLabel ."
    },
    {
      "name": "ask-soil-operations",
      "type": "ask",
      "prefixes": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
        "om": "http://www.ontology-of-units-of-measure.org/resource/om-2/"
      },
      "vars": ["operation", "soilManagementOperationType", "parcel", "applied", "appliedUnit", "appliedValue", "area", "areaUnit", "areaValue", "start", "end", "agent", "material", "activeSubstance"],
      "pattern": "?operation rdf:type ?soilManagementOperationType . ?soilManagementOperationType rdfs:subClassOf ploutos:SoilManagementOperation . ?operation ploutos:isOperatedOn ?parcel . ?operation ploutos:hasAppliedAmount ?applied . ?applied om:hasUnit ?appliedUnit . ?applied om:hasNumericalValue ?appliedValue . ?operation ploutos:isAppliedPer ?area . ?area om:hasUnit ?areaUnit . ?area om:hasNumericalValue ?areaValue . ?operation ploutos:hasStartDatetime ?start . ?operation ploutos:hasEndDatetime ?end . ?operation ploutos:responsibleAgent ?agent . ?operation ploutos:usesMaterial ?material . ?material ploutos:hasActiveSubstance ?activeSubstance ."
    },
    {
      "name": "ask-global-gap-number",
      "type": "ask",
      "prefixes": {
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#"
      },
      "vars": ["organisation", "ggn"],
      "pattern": "?organisation ploutos:hasGlobalGapNumber ?ggn ."
    },
    {
      "name": "ask-certificates",
      "type": "ask",
      "prefixes": {
        "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
        "ploutos": "https://www.tno.nl/agrifood/ontology/ploutos/common#",
        "dcterms": "http://purl.org/dc/terms/"
      },
      "vars": ["certificate", "certificateSource", "asci", "asciId", "asciProvider", "organisation", "asoi", "asoiId", "asoiProvider", "scheme", "validFrom", "validUntil", "productClass"],
      "pattern": "?certificate rdf:type ploutos:Certificate . ?certificate dcterms:source ?certificateSource . ?certificate ploutos:hasAuthoritySpecificIdentifier ?asci . ?asci ploutos:hasIdentifier ?asciId . ?asci ploutos:isProvidedBy ?asciProvider . ?certificate ploutos:hasCertifiedOrganization ?organisation . ?organisation ploutos:hasAuthoritySpecificIdentifier ?asoi . ?asoi ploutos:hasIdentifier ?asoiId . ?asoi ploutos:isProvidedBy ?asoiProvider . ?certificate ploutos:hasCertificationScheme ?scheme . ?certificate ploutos:isValidFrom ?validFrom . ?certificate ploutos:isValidUntil ?validUntil . ?certificate ploutos:hasCertifiedObject ?productClass ."
    }
  ]
}
